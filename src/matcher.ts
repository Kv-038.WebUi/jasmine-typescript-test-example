let customMatchers: jasmine.CustomMatcherFactories = {
    toBeBeBe: function (util: jasmine.MatchersUtil, customEqualityTesters: Array<jasmine.CustomEqualityTester>): jasmine.CustomMatcher {
        return {
            compare: function (actual: any, expected: any): jasmine.CustomMatcherResult {
                // Set pass to true to stop failing :)
                const result: jasmine.CustomMatcherResult = {
                    pass: false,
                    message: 'Cool Message'
                };

                return result;
            }
        }
    }
};

export { customMatchers };