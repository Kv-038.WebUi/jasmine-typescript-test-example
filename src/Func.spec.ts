import {helloWorld} from "./Func";
import { customMatchers } from './matcher'

describe("Testing SomeClass functions", function() {
    beforeEach(() => {
        jasmine.addMatchers(customMatchers);
    });
    it("says hello", function() {
        expect(helloWorld()).toEqual("Hello World!");
    });

    it("Check if be be be :)", function() {
        expect(helloWorld()).toBeBeBe();
    });
});